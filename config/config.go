package config

import (
	"log"
	"os"
	"sync"

	"github.com/joho/godotenv"
	"go.uber.org/ratelimit"
)

var once sync.Once

type Config struct {
	DB             *DBConfig
	SMSRateLimiter ratelimit.Limiter
}

type DBConfig struct {
	Dialect  string
	Username string
	Password string
	Name     string
	Host     string
	Port     string
}

var instance *Config

func GetConfig() *Config {
	once.Do(func() {
		var err error
		env_file := "/app/.env"

		if os.Getenv("MODE") == "testing" {
			env_file = "/app/.env.test"
		}

		err = godotenv.Load(env_file)
		if err != nil {
			log.Fatalf("Error getting env, %v", err)
		}

		instance = &Config{
			SMSRateLimiter: ratelimit.New(100),
			DB: &DBConfig{
				Dialect:  "postgres",
				Username: os.Getenv("DB_USER"),
				Password: os.Getenv("DB_PASSWORD"),
				Name:     os.Getenv("DB_NAME"),
				Host:     os.Getenv("DB_HOST"),
				Port:     os.Getenv("DB_PORT"),
			},
		}
	})

	return instance
}
