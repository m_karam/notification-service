module notification-service

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.5.1 // indirect
	go.uber.org/ratelimit v0.1.0
)
