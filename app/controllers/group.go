package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/jinzhu/gorm"
	"notification-service/app/models"
	"notification-service/app/repositories"
)

func GetGroups(w http.ResponseWriter, r *http.Request, groupRepo repositories.Group) {
	groups, err := groupRepo.GetAllGroups()

	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	respondJSON(w, http.StatusOK, groups)
}

func CreateGroup(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	group := models.Group{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&group); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	defer r.Body.Close()

	if err := db.Save(&group).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, group)
}
