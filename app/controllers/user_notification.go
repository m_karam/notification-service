package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"notification-service/app/models"
	"notification-service/app/repositories"
	"notification-service/app/services"
)

func GetUserNotifications(w http.ResponseWriter, r *http.Request, userNotificationRepo repositories.UserNotification) {
	vars := mux.Vars(r)
	userNotifications, err := userNotificationRepo.GetUserNotificationsByUserID(vars["userID"])

	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	respondJSON(w, http.StatusOK, userNotifications)
}

func CreateUserNotification(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	userNotification := models.UserNotification{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&userNotification); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	defer r.Body.Close()

	if err := db.Save(&userNotification).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusOK, userNotification)
}

func SendUserNotification(w http.ResponseWriter, r *http.Request, userNotificationRepo repositories.UserNotification) {
	vars := mux.Vars(r)
	userNotification, err := userNotificationRepo.GetUserNotification(vars["id"])

	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	go services.SendUserNotificationFactory(&userNotification)
	respondJSON(w, http.StatusOK, true)
}
