package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/jinzhu/gorm"
	"notification-service/app/models"
	"notification-service/app/repositories"
)

func GetNotifications(w http.ResponseWriter, r *http.Request, notificationRepo repositories.Notification) {
	notifications, err := notificationRepo.GetAllNotifications()

	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	respondJSON(w, http.StatusOK, notifications)
}

func CreateNotification(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	notification := models.Notification{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&notification); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	defer r.Body.Close()

	if err := db.Save(&notification).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, notification)
}
