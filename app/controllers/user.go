package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/jinzhu/gorm"
	"notification-service/app/models"
	"notification-service/app/repositories"
)

func GetUsers(w http.ResponseWriter, r *http.Request, userRepo repositories.User) {
	users, err := userRepo.GetAllUsers()

	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	respondJSON(w, http.StatusOK, users)
}

func CreateUser(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	user := models.User{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&user); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	defer r.Body.Close()

	if err := db.Save(&user).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, user)
}
