package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"notification-service/app/models"
	"notification-service/app/repositories"
	"notification-service/app/services"
)

func GetGroupNotifications(w http.ResponseWriter, r *http.Request, groupNotificationRepo repositories.GroupNotification) {
	groupNotifications, err := groupNotificationRepo.GetAllGroupNotifications()

	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	respondJSON(w, http.StatusOK, groupNotifications)
}

func CreateGroupNotification(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	groupNotification := models.GroupNotification{}
	decoder := json.NewDecoder(r.Body)

	if err := decoder.Decode(&groupNotification); err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	defer r.Body.Close()

	if err := db.Save(&groupNotification).Error; err != nil {
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusOK, groupNotification)
}

func SendGroupNotification(w http.ResponseWriter, r *http.Request, groupNotificationRepo repositories.GroupNotification) {
	vars := mux.Vars(r)
	groupNotification, err := groupNotificationRepo.GetGroupNotification(vars["id"])

	if err != nil {
		respondError(w, http.StatusBadRequest, err.Error())
		return
	}

	go services.SendGroupNotificationFactory(&groupNotification)
	respondJSON(w, http.StatusOK, true)
}
