package app

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"notification-service/app/controllers"
	"notification-service/app/db"
	"notification-service/app/repositories"
	"notification-service/config"
)

// App has router and db instances
type App struct {
	Router *mux.Router
	DB     *gorm.DB
}

// App initialize with predefined configuration
func (a *App) Initialize(config *config.Config) {
	a.DB = db.Init(config)
	a.Router = mux.NewRouter()
	a.setRouters()
}

// Set all required routers
func (a *App) setRouters() {
	a.Get("/notifications", func(w http.ResponseWriter, r *http.Request) {
		notificationRepo := repositories.NewNotification(a.DB)
		controllers.GetNotifications(w, r, notificationRepo)
	})
	a.Post("/notifications", func(w http.ResponseWriter, r *http.Request) {
		controllers.CreateNotification(w, r, a.DB)
	})
	a.Get("/group-notifications", func(w http.ResponseWriter, r *http.Request) {
		groupNotificationRepo := repositories.NewGroupNotification(a.DB)
		controllers.GetGroupNotifications(w, r, groupNotificationRepo)
	})
	a.Post("/group-notifications", func(w http.ResponseWriter, r *http.Request) {
		controllers.CreateGroupNotification(w, r, a.DB)
	})
	a.Post("/group-notifications/{id}/send", func(w http.ResponseWriter, r *http.Request) {
		groupNotificationRepo := repositories.NewGroupNotification(a.DB)
		controllers.SendGroupNotification(w, r, groupNotificationRepo)
	})
	a.Get("/user-notifications/{userID}", func(w http.ResponseWriter, r *http.Request) {
		userNotificationRepo := repositories.NewUserNotification(a.DB)
		controllers.GetUserNotifications(w, r, userNotificationRepo)
	})
	a.Post("/user-notifications", func(w http.ResponseWriter, r *http.Request) {
		controllers.CreateUserNotification(w, r, a.DB)
	})
	a.Post("/user-notifications/{id}/send", func(w http.ResponseWriter, r *http.Request) {
		userNotificationRepo := repositories.NewUserNotification(a.DB)
		controllers.SendUserNotification(w, r, userNotificationRepo)
	})
	a.Get("/users", func(w http.ResponseWriter, r *http.Request) {
		userRepo := repositories.NewUser(a.DB)
		controllers.GetUsers(w, r, userRepo)
	})
	a.Post("/users", func(w http.ResponseWriter, r *http.Request) {
		controllers.CreateUser(w, r, a.DB)
	})
	a.Get("/groups", func(w http.ResponseWriter, r *http.Request) {
		groupRepo := repositories.NewGroup(a.DB)
		controllers.GetGroups(w, r, groupRepo)
	})
	a.Post("/groups", func(w http.ResponseWriter, r *http.Request) {
		controllers.CreateGroup(w, r, a.DB)
	})
}

// Run the app on it's router
func (a *App) Run(host string) {
	log.Fatal(http.ListenAndServe(host, a.Router))
}
