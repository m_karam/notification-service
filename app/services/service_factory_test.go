package services

import (
	"notification-service/app/models"
	"testing"
)

var enUserMock = models.User{
	PrefferedLang: "EN",
	FirstName:     "Mohamed",
	LastName:      "Karam",
	PhoneNumber:   "0123456789",
	DeviceToken:   "1234-5678-9101-1121",
}

var arUserMock = models.User{
	PrefferedLang: "AR",
	FirstName:     "Mohamed",
	LastName:      "Karam",
	PhoneNumber:   "0113456789",
	DeviceToken:   "1121-5678-9101-1234",
}

var notificationMock = models.Notification{
	TextAr: "مرحبا",
	TextEn: "Hello",
}

var groupMock = models.Group{
	Name:  "All users",
	Topic: "all-users",
	Users: []*models.User{
		&enUserMock,
		&arUserMock,
	},
}

var smsGroupNotificationMock = models.GroupNotification{
	DeliveryType: "SMS",
	Notification: notificationMock,
	Groups: []models.Group{
		groupMock,
	},
}

var smsUserNotificationMock = models.UserNotification{
	DeliveryType: "SMS",
	Notification: notificationMock,
	User:         enUserMock,
}

var pushGroupNotificationMock = models.GroupNotification{
	DeliveryType: "Push",
	Notification: notificationMock,
	Groups: []models.Group{
		groupMock,
	},
}

var pushUserNotificationMock = models.UserNotification{
	DeliveryType: "Push",
	Notification: notificationMock,
	User:         enUserMock,
}

func TestGetLocalizedNotificationText(t *testing.T) {
	tables := []struct {
		x      models.User
		y      models.Notification
		result string
	}{
		{enUserMock, notificationMock, notificationMock.TextEn},
		{arUserMock, notificationMock, notificationMock.TextAr},
	}

	for _, table := range tables {
		localizedText := getLocalizedNotificationText(&table.x, &table.y)
		if localizedText != table.result {
			t.Errorf("Wrong localized text, expected %s", table.result)
		}
	}
}
