package services

import (
	"bytes"
	"log"

	"testing"
)

func TestSendGroupPushNotification(t *testing.T) {
	var str bytes.Buffer
	var outputMock bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&str)
	pushService := &PushNotificationService{}
	pushService.SendGroupPushNotification(&pushGroupNotificationMock)
	log.SetOutput(&outputMock)
	text := notificationLocalizedText{
		en: notificationMock.TextEn,
		ar: notificationMock.TextAr,
	}
	log.Println("Message: ", text, "Receiver: ", groupMock.Topic)
	if str.String() != outputMock.String() {
		t.Errorf("Wrong notification sent")
	}
}

func TestSendUserPushNotification(t *testing.T) {
	var str bytes.Buffer
	var outputMock bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&str)
	pushService := &PushNotificationService{}
	pushService.SendUserPushNotification(&pushUserNotificationMock)
	log.SetOutput(&outputMock)
	localizedText := getLocalizedNotificationText(&pushUserNotificationMock.User, &pushUserNotificationMock.Notification)
	log.Println("Message: ", localizedText, "Receiver: ", pushUserNotificationMock.User.DeviceToken)
	if str.String() != outputMock.String() {
		t.Errorf("Wrong notification sent")
	}
}
