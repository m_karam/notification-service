package services

import (
	"notification-service/app/models"
)

func SendGroupNotificationFactory(notification *models.GroupNotification) {
	if notification.DeliveryType == "Push" {
		pushService := &PushNotificationService{}
		pushService.SendGroupPushNotification(notification)
	} else if notification.DeliveryType == "SMS" {
		smsService := NewSMSNotificationService()
		smsService.SendGroupSMSNotification(notification)
	}
}

func SendUserNotificationFactory(notification *models.UserNotification) {
	if notification.DeliveryType == "Push" {
		pushService := &PushNotificationService{}
		pushService.SendUserPushNotification(notification)
	} else if notification.DeliveryType == "SMS" {
		smsService := NewSMSNotificationService()
		smsService.SendUserSMSNotification(notification)
	}
}

func getLocalizedNotificationText(user *models.User, notification *models.Notification) string {
	if user.PrefferedLang == "AR" {
		return notification.TextAr
	}

	return notification.TextEn
}
