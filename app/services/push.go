package services

import (
	"log"

	"notification-service/app/models"
)

type notificationLocalizedText struct {
	en string `json:"en"`
	ar string `json:"ar"`
}

type PushNotificationService struct {
}

func (p *PushNotificationService) SendGroupPushNotification(groupNotification *models.GroupNotification) {
	for _, group := range groupNotification.Groups {
		textStruct := notificationLocalizedText{
			en: groupNotification.Notification.TextEn,
			ar: groupNotification.Notification.TextAr,
		}
		p.sendPushToTopic(textStruct, group.Topic)
	}
}

func (p *PushNotificationService) SendUserPushNotification(userNotification *models.UserNotification) {
	localizedText := getLocalizedNotificationText(&userNotification.User, &userNotification.Notification)
	p.sendPushToDevice(localizedText, userNotification.User.DeviceToken)
}

func (p *PushNotificationService) sendPushToDevice(text string, deviceToken string) {
	log.SetFlags(0)
	log.Println("Message: ", text, "Receiver: ", deviceToken)
}

func (p *PushNotificationService) sendPushToTopic(text notificationLocalizedText, topicName string) {
	log.SetFlags(0)
	log.Println("Message: ", text, "Receiver: ", topicName)
}
