package services

import (
	"bytes"
	"log"
	"testing"
)

func TestSendUserSMSNotification(t *testing.T) {
	var str bytes.Buffer
	var outputMock bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&str)
	smsService := NewSMSNotificationService()
	smsService.SendUserSMSNotification(&smsUserNotificationMock)
	localizedText := getLocalizedNotificationText(&smsUserNotificationMock.User, &smsUserNotificationMock.Notification)
	log.SetOutput(&outputMock)
	log.Println("Message: ", localizedText, "Receiver: ", smsUserNotificationMock.User.PhoneNumber)
	if str.String() != outputMock.String() {
		t.Errorf("Wrong notification sent")
	}
}

func TestSendGroupSMSNotification(t *testing.T) {
	var str bytes.Buffer
	var outputMock bytes.Buffer
	log.SetFlags(0)
	log.SetOutput(&str)
	smsService := NewSMSNotificationService()
	smsService.SendGroupSMSNotification(&smsGroupNotificationMock)
	log.SetOutput(&outputMock)
	for _, user := range groupMock.Users {
		localizedText := getLocalizedNotificationText(user, &smsGroupNotificationMock.Notification)
		log.Println("Message: ", localizedText, "Receiver: ", user.PhoneNumber)
	}
	if str.String() != outputMock.String() {
		t.Errorf("Wrong notification sent")
	}
}
