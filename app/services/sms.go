package services

import (
	"log"

	"notification-service/app/models"
	"notification-service/config"
)

type SMSNotificationService interface {
	SendGroupSMSNotification(groupNotification *models.GroupNotification)
	SendUserSMSNotification(userNotification *models.UserNotification)
	sendSMS(text string, phoneNumber string)
}

type smsNotificationServiceImpl struct {
}

func NewSMSNotificationService() SMSNotificationService {
	return smsNotificationServiceImpl{}
}

func (s smsNotificationServiceImpl) SendGroupSMSNotification(groupNotification *models.GroupNotification) {
	for _, group := range groupNotification.Groups {
		for _, user := range group.Users {
			localizedText := getLocalizedNotificationText(user, &groupNotification.Notification)
			s.sendSMS(localizedText, user.PhoneNumber)
		}
	}
}

func (s smsNotificationServiceImpl) SendUserSMSNotification(userNotification *models.UserNotification) {
	localizedText := getLocalizedNotificationText(&userNotification.User, &userNotification.Notification)
	s.sendSMS(localizedText, userNotification.User.PhoneNumber)
}

func (s smsNotificationServiceImpl) sendSMS(text string, phoneNumber string) {
	conf := config.GetConfig()
	conf.SMSRateLimiter.Take()
	log.SetFlags(0)
	log.Println("Message: ", text, "Receiver: ", phoneNumber)

}
