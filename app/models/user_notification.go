package models

import "github.com/jinzhu/gorm"

// UserNotification entity
type UserNotification struct {
	gorm.Model
	UserID         uint
	User           User
	NotificationID uint
	Notification   Notification
	DeliveryType   string
}
