package models

import "github.com/jinzhu/gorm"

// Group entity
type Group struct {
	gorm.Model
	Name  string
	Topic string
	Users []*User `gorm:"many2many:user_groups;association_autoupdate:false"`
}
