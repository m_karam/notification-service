package models

import "github.com/jinzhu/gorm"

// User entity
type User struct {
	gorm.Model
	FirstName         string
	LastName          string
	PhoneNumber       string
	PrefferedLang     string
	DeviceToken       string
	UserNotifications []UserNotification
	Groups            []*Group `gorm:"many2many:user_groups;association_autoupdate:false;auto_preload"`
}
