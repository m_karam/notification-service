package models

import "github.com/jinzhu/gorm"

// GroupNotification entity
type GroupNotification struct {
	gorm.Model
	Groups         []Group `gorm:"many2many:notification_groups;association_autoupdate:false"`
	DeliveryType   string
	NotificationID uint
	Notification   Notification
}
