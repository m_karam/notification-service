package models

import "github.com/jinzhu/gorm"

// Notification entity
type Notification struct {
	gorm.Model
	TextAr string
	TextEn string
}
