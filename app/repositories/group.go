package repositories

import (
	"github.com/jinzhu/gorm"
	"notification-service/app/models"
)

type Group interface {
	GetGroup(groupID string) (models.Group, error)
	GetAllGroups() ([]models.Group, error)
}

type groupImpl struct {
	db *gorm.DB
}

func NewGroup(db *gorm.DB) Group {
	return groupImpl{db}
}

func (repo groupImpl) GetGroup(groupID string) (models.Group, error) {
	group := models.Group{}
	err := repo.db.First(&group, groupID).Error

	return group, err
}

func (repo groupImpl) GetAllGroups() ([]models.Group, error) {
	groups := []models.Group{}
	err := repo.db.Find(&groups).Error

	return groups, err
}

func (repo groupImpl) GetUsers(group *models.Group) ([]models.User, error) {
	users := []models.User{}
	err := repo.db.Model(&group).Association("Users").Find(&users).Error

	return users, err
}
