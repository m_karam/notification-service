package repositories

import (
	"github.com/jinzhu/gorm"
	"notification-service/app/models"
)

type Notification interface {
	GetNotification(notificationID string) (models.Notification, error)
	GetAllNotifications() ([]models.Notification, error)
}

type notificationImpl struct {
	db *gorm.DB
}

func NewNotification(db *gorm.DB) Notification {
	return notificationImpl{db}
}

func (repo notificationImpl) GetNotification(notificationID string) (models.Notification, error) {
	notification := models.Notification{}
	err := repo.db.First(&notification, notificationID).Error

	return notification, err
}

func (repo notificationImpl) GetAllNotifications() ([]models.Notification, error) {
	notifications := []models.Notification{}
	err := repo.db.Find(&notifications).Error

	return notifications, err
}
