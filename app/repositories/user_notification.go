package repositories

import (
	"github.com/jinzhu/gorm"
	"notification-service/app/models"
)

type UserNotification interface {
	GetUserNotification(userNotificationID string) (models.UserNotification, error)
	GetUserNotificationsByUserID(userID string) ([]models.UserNotification, error)
}

type userNotificationImpl struct {
	db *gorm.DB
}

func NewUserNotification(db *gorm.DB) UserNotification {
	return userNotificationImpl{db}
}

func (repo userNotificationImpl) GetUserNotification(userNotificationID string) (models.UserNotification, error) {
	userNotification := models.UserNotification{}
	err := repo.db.Preload("Notification").Preload("User").First(&userNotification, userNotificationID).Error

	return userNotification, err
}

func (repo userNotificationImpl) GetUserNotificationsByUserID(userID string) ([]models.UserNotification, error) {
	userNotifications := []models.UserNotification{}
	err := repo.db.Preload("Notification").Preload("User").Where("user_id = ?", userID).Find(&userNotifications).Error

	return userNotifications, err
}
