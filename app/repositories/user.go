package repositories

import (
	"github.com/jinzhu/gorm"
	"notification-service/app/models"
)

type User interface {
	GetUser(userID string) (models.User, error)
	GetAllUsers() ([]models.User, error)
}

type userImpl struct {
	db *gorm.DB
}

func NewUser(db *gorm.DB) User {
	return userImpl{db}
}

func (repo userImpl) GetUser(userID string) (models.User, error) {
	user := models.User{}
	err := repo.db.First(&user, userID).Error

	return user, err
}

func (repo userImpl) GetAllUsers() ([]models.User, error) {
	users := []models.User{}
	err := repo.db.Find(&users).Error

	return users, err
}
