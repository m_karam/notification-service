package repositories

import (
	"github.com/jinzhu/gorm"
	"notification-service/app/models"
)

type GroupNotification interface {
	GetGroupNotification(groupNotificationID string) (models.GroupNotification, error)
	GetAllGroupNotifications() ([]models.GroupNotification, error)
}

type groupNotificationImpl struct {
	db *gorm.DB
}

func NewGroupNotification(db *gorm.DB) GroupNotification {
	return groupNotificationImpl{db}
}

func (repo groupNotificationImpl) GetGroupNotification(groupNotificationID string) (models.GroupNotification, error) {
	groupNotification := models.GroupNotification{}
	err := repo.db.Preload("Notification").Preload("Groups.Users").First(&groupNotification, groupNotificationID).Error

	return groupNotification, err
}

func (repo groupNotificationImpl) GetAllGroupNotifications() ([]models.GroupNotification, error) {
	groupNotifications := []models.GroupNotification{}
	err := repo.db.Preload("Notification").Preload("Groups.Users").Find(&groupNotifications).Error

	return groupNotifications, err
}
