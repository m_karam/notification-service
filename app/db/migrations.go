package db

import (
	"github.com/jinzhu/gorm"
	"notification-service/app/models"
)

// DBMigrate will create and migrate the tables, and then make the some relationships if necessary
func DBMigrate(db *gorm.DB) *gorm.DB {
	db.AutoMigrate(
		&models.GroupNotification{},
		&models.Notification{},
		&models.User{},
		&models.UserNotification{},
		&models.Group{},
	)
	return db
}
