# Notification Service

## Installation
You need to install `docker` and`docker-compose` then execute the following:

- Clone this repository
- Run command `docker-compose up -d` to run the development environment
- Use `http://localhost:3000` as your base url

## Running tests
To run tests, use the following command
`docker-compose -f docker-compose.test.yml up`

## API Documentation
This service provides RESTful APIs.

*TODO: Use swagger*

### Get Notifications

Used to get list of notifications.

**URL** : `/notifications`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content example**

```json
[
  {
    "ID": 1,
    "CreatedAt": "2020-03-29T05:29:45.97433Z",
    "UpdatedAt": "2020-03-29T05:29:45.97433Z",
    "DeletedAt": null,
    "TextAr": "مرحبا",
    "TextEn": "Hello"
  }
]
```

### Create Notifications

Used to create a notification.

**URL** : `/notifications`

**Method** : `POST`

**Data constraints**

```json
{
    "TextEn": "[text]",
    "TextAr": "[text]"
}
```

**Data example**

```json
{
    "TextEn": "Hello",
    "TextAr": "مرحبا"
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "ID": 1,
    "CreatedAt": "2020-03-29T05:29:45.97433Z",
    "UpdatedAt": "2020-03-29T05:29:45.97433Z",
    "DeletedAt": null,
    "TextAr": "مرحبا",
    "TextEn": "Hello"
}
```
### Create Group

Used to create a group.

**URL** : `/groups`

**Method** : `POST`

**Data constraints**

```json
{
  "Name": "[text]",
  "Topic": "[text]"
}
```

**Data example**

```json
{
  "Name": "All Users",
  "Topic": "all-users"
}
```
### GET Groups

Used to get list of groups.

**URL** : `/groups`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content example**

```json
[
  {
    "ID": 1,
    "CreatedAt": "2020-03-29T05:30:29.91759Z",
    "UpdatedAt": "2020-03-29T05:30:29.91759Z",
    "DeletedAt": null,
    "Name": "All Users",
    "Topic": "all-users",
  }
]
```

### Create User

Used to create a user.

**URL** : `/users`

**Method** : `POST`

**Data constraints**

```json
{
  "FirstName": "[text]",
  "LastName": "[text]",
  "PhoneNumber": "[text]",
  "PrefferedLang": "[text]", // AR or EN
  "DeviceToken": "[text]",
}
```

**Data example**

```json
{
  "FirstName": "Mohamed",
  "LastName": "Karam",
  "PhoneNumber": "0123456789",
  "PrefferedLang": "AR", // AR or EN
  "DeviceToken": "1234-5678-91011",
}
```
### GET Users

Used to get list of users.

**URL** : `/users`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content example**

```json
[
  {
    "ID": 1,
    "CreatedAt": "2020-03-29T05:40:07.90523Z",
    "UpdatedAt": "2020-03-29T05:40:07.90523Z",
    "DeletedAt": null,
    "Groups": [
      {
        "ID": 1,
        "CreatedAt": "2020-03-29T05:30:29.91759Z",
        "UpdatedAt": "2020-03-29T05:30:29.91759Z",
        "DeletedAt": null,
        "Name": "All Users",
        "Topic": "all-users",
      }
    ],
    "DeliveryType": "Push",
    "NotificationID": 1,
    "Notification": {
      "ID": 1,
      "CreatedAt": "2020-03-29T05:29:45.97433Z",
      "UpdatedAt": "2020-03-29T05:29:45.97433Z",
      "DeletedAt": null,
      "TextAr": "مرحبا",
      "TextEn": "Hello"
    }
  }
]
```

### Create Group Notifications

Used to create a notification.

**URL** : `/group-notifications`

**Method** : `POST`

**Data constraints**

```json
{
  "DeliveryType": "[text]", // SMS or Push
  "NotificationID": 1, // ID of a notification
  "Groups": [ // List of groups that the notification will be sent to
    {
      "ID": 1, // ID of a group
    }
  ]
}
```

**Data example**

```json
{
    "DeliveryType": "SMS",
    "NotificationID": 1,
    "Groups": [
      {
        "ID": 1,
      }
    ]
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "ID": 1,
  "CreatedAt": "2020-03-29T05:40:07.90523Z",
  "UpdatedAt": "2020-03-29T05:40:07.90523Z",
  "DeletedAt": null,
  "Groups": [
    {
      "ID": 1,
      "CreatedAt": "2020-03-29T05:30:29.91759Z",
      "UpdatedAt": "2020-03-29T05:30:29.91759Z",
      "DeletedAt": null,
      "Name": "All Users",
      "Topic": "all-users"
    }
  ],
  "DeliveryType": "Push",
  "NotificationID": 1,
  "Notification": {
    "ID": 1,
    "CreatedAt": "2020-03-29T05:29:45.97433Z",
    "UpdatedAt": "2020-03-29T05:29:45.97433Z",
    "DeletedAt": null,
    "TextAr": "مرحبا",
    "TextEn": "Hello"
  }
}
```

### Get Group Notifications

Used to get list of group notifications.

**URL** : `/group-notifications`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content example**

```json
[
  {
    "ID": 1,
    "CreatedAt": "2020-03-29T05:40:07.90523Z",
    "UpdatedAt": "2020-03-29T05:40:07.90523Z",
    "DeletedAt": null,
    "Groups": [
      {
        "ID": 1,
        "CreatedAt": "2020-03-29T05:30:29.91759Z",
        "UpdatedAt": "2020-03-29T05:30:29.91759Z",
        "DeletedAt": null,
        "Name": "All Users",
        "Topic": "all-users",
      }
    ],
    "DeliveryType": "Push",
    "NotificationID": 1,
    "Notification": {
      "ID": 1,
      "CreatedAt": "2020-03-29T05:29:45.97433Z",
      "UpdatedAt": "2020-03-29T05:29:45.97433Z",
      "DeletedAt": null,
      "TextAr": "مرحبا",
      "TextEn": "Hello"
    }
  }
]
```

### Create User Notifications

Used to create a notification for a specific user.

**URL** : `/user-notifications`

**Method** : `POST`

**Data constraints**

```json
{
  "DeliveryType": "[text]", // SMS or Push
  "NotificationID": 1, // ID of a notification
  "UserID": 1, // ID of a user
}
```

**Data example**

```json
{ 
  "DeliveryType": "Push",
  "NotificationID": 1,
  "UserID": 1
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
  "ID": 1,
  "CreatedAt": "2020-03-29T05:42:25.053738Z",
  "UpdatedAt": "2020-03-29T05:42:25.053738Z",
  "DeletedAt": null,
  "UserID": 1,
  "User": {
    "ID": 1,
    "CreatedAt": "2020-03-29T05:35:17.912823Z",
    "UpdatedAt": "2020-03-29T05:35:17.912823Z",
    "DeletedAt": null,
    "FirstName": "Mohamed",
    "LastName": "Karam",
    "PhoneNumber": "0123456789",
    "PrefferedLang": "EN",
    "DeviceToken": "abcd-122145645-opc",
  },
  "NotificationID": 1,
  "Notification": {
    "ID": 1,
    "CreatedAt": "2020-03-29T05:29:45.97433Z",
    "UpdatedAt": "2020-03-29T05:29:45.97433Z",
    "DeletedAt": null,
    "TextAr": "مرحبا",
    "TextEn": "Hello"
  },
  "DeliveryType": "Push"
}
```

### Get User Notifications

Used to get list of notifications for a specific user.

**URL** : `/user-notifications/{userID}`

**Method** : `GET`

#### Success Response

**Code** : `200 OK`

**Content example**

```json
[
  {
    "ID": 1,
    "CreatedAt": "2020-03-29T05:42:25.053738Z",
    "UpdatedAt": "2020-03-29T05:42:25.053738Z",
    "DeletedAt": null,
    "UserID": 1,
    "User": {
      "ID": 1,
      "CreatedAt": "2020-03-29T05:35:17.912823Z",
      "UpdatedAt": "2020-03-29T05:35:17.912823Z",
      "DeletedAt": null,
      "FirstName": "Mohamed",
      "LastName": "Karam",
      "PhoneNumber": "0123456789",
      "PrefferedLang": "EN",
      "DeviceToken": "abcd-122145645-opc"
    },
    "NotificationID": 1,
    "Notification": {
      "ID": 1,
      "CreatedAt": "2020-03-29T05:29:45.97433Z",
      "UpdatedAt": "2020-03-29T05:29:45.97433Z",
      "DeletedAt": null,
      "TextAr": "مرحبا",
      "TextEn": "Hello"
    },
    "DeliveryType": "Push"
  }
]
```

### Send Group Notification

Used to create a notification for a specific user.

**URL** : `/group-notifications/{id}/send`

**Method** : `POST`

## Success Response

**Code** : `200 OK`

**Content example**

```json
true
```

### Send User Notification

Used to create a notification for a specific user.

**URL** : `/user-notifications/{id}/send`

**Method** : `POST`

## Success Response

**Code** : `200 OK`

**Content example**

```json
true
```